$(document).ready(function () {
    $('button[name=addUser]').on('click', function () {
        $error = '';
        $('#notification').html($error);
        if ($('#addEmployeeModal input[name=userName]').val() == '') $error += "<li>Username cannot be left blank</li>";
        if ($('#addEmployeeModal input[name=firstName]').val() == '') $error += "<li>First Name cannot be left blank</li>";
        if ($('#laddEmployeeModa input[name=lastName]').val() == '') $error += "<li>Last Name cannot be left blank</li>";
        if ($('#addEmployeeModal input[name=email]').val() == '') $error += "<li>Email cannot be left blank</li>";
        if ($('#addEmployeeModal input[name=psw]').val() == '') $error += "<li>Password cannot be left blank</li>";
        if ($error != '') {
            $('#notification').html($error);
        }
        else {
            $userData = new FormData($('#addEmployeeModal form')[0]);
            $userData.append('type', 'addUser');
            $.ajax({
                url: 'addUser.php',
                type: 'post',
                processData: false,
                contentType: false,
                data: $userData,
                success: function (response) {
                    if (response == 1) {
                        $('#addEmployeeModal').modal('hide');
                        $content = '<h1 class="text-success">Add user Successed!</h1><p class="decs"> The activation mail is sent to user&#39;s email. User have to click the activation link to activate account.</p>';
                        $('#notification').html($content);
                        // console.log('true');
                    }
                    else {
                        $('#notification').html(response);
                    }
                }
            });
        }
    });
});