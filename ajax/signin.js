// AJAX login
$(document).ready(function () {
    $("#BTNsignIn").on("click", function () {
        var userName = $("#userName").val();
        var passWord = $("#pwd").val();
        var reCAPTCHA = grecaptcha.getResponse();
        if (reCAPTCHA != '') {
            $.ajax({
                type: 'get',
                data: {
                    type: 'reCAPTCHA',
                    response: reCAPTCHA
                },
                success: function (result) {
                    if (result == 1) {
                        if (userName != '' && passWord != '') {
                            $.ajax({
                                url: "signIn.php",
                                dataType: 'json',
                                method: "POST",
                                data: {
                                    type: 'signIn',
                                    userName: userName,
                                    passWord: passWord
                                },
                                success: function (response) {
                                    if (response['success'] == true)
                                        window.location.href = "/phpbasic3/";
                                    else {
                                        // console.log(response);
                                        // var result = JSON.parse(response);
                                        $('#error').show();
                                        $('#error').html(response['error']);
                                    }
                                }
                            });
                        }
                        else {
                            $('#error').show();
                            $('#error').html('<li>Something is invalid.</li>');
                        }
                    }
                }
            });
        };

    });
});

