$(document).ready(function () {
    $('#BTNdelUser').on('click', function () {
        $userID = $('#userID').val();
        $pageID = $('input[name=pageID').val();
        $.ajax({
            url: 'delUser.php',
            type: 'get',
            data: {
                type: 'delUser',
                userID: $userID,
                pageID: $pageID
            },
            success: function (response) {
                if (response == 1) {
                    $node = document.getElementsByTagName('tr')[1];
                    $("tr").remove("#" + $userID);
                    $('#deleteEmployeeModal').modal('hide');
                    if (!$.contains(document.body, $node))
                        window.location.replace('/phpbasic3/admin/listUser.php?page=' + ($pageID - 1));
                }
                else {
                    console.log('flase');
                }
            }
        });
    });
    $('#delMulUser').on('click', function () {
        $pageID = $('input[name=pageID').val();
        $checkedArray = [];
        $('input[name="userCheckbox[]"]:checked').each(function (i) {
            $checkedArray[i] = $(this).val();
        });
        $.ajax({
            url: 'delUser.php',
            type: 'get',
            data: {
                type: 'delMulUser',
                list: $checkedArray
            },
            success: function (response) {
                if (response == 1) {
                    $node = document.getElementsByTagName('tr')[1];
                    $.each($checkedArray, function (i) {
                        $('tr').remove('#' + $checkedArray[i]);
                    });
                    $('#deleteMultipleEmployeeModal').modal('hide');
                    if (!$.contains(document.body, $node))
                        window.location.replace('/phpbasic3/admin/listUser.php?page=' + ($pageID - 1));

                } else {
                    $('#notification').html(response);
                }
            }
        });
    });
});
