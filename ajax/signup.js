// AJAX signUp

$(document).ready(function () {
    $("#BTNsignUp").on("click", function () {
        var reCAPTCHA = grecaptcha.getResponse();
        $error = '';
        $('#notification').html($error);
        $userData = new FormData($('form[name=formSignUp]')[0]);
        if ($('#userName').val() == '') $error += "<li>Username cannot be left blank</li>";
        if ($('#firstName').val() == '') $error += "<li>First Name cannot be left blank</li>";
        if ($('#lastName').val() == '') $error += "<li>Last Name cannot be left blank</li>";
        if ($('#email').val() == '') $error += "<li>Email cannot be left blank</li>";
        if ($('#psw').val() == '') $error += "<li>Password cannot be left blank</li>";
        if ($('#psw').val() != $('#psw-repeat').val()) $error += "<li>Password and repeat-password not match</li>";
        if ($error != '') {
            $('#notification').html($error);
        }
        else {
            if (reCAPTCHA != '') {
                $.ajax({
                    type: 'get',
                    data: {
                        type: 'reCAPTCHA',
                        response: reCAPTCHA
                    },
                    success: function (result) {
                        $userData = new FormData($('form[name=formSignUp]')[0]);
                        $userData.append('type', 'registerUser');
                        $.ajax({
                            url: 'signUp.php',
                            type: 'post',
                            // dataType: 'json',
                            processData: false,
                            contentType: false,
                            data: $userData,
                            success: function (response) {
                                if (response == 1) {
                                    $('#formSignUp').hide();
                                    $content = '<h1 class="text-success">Register Successed!</h1><p class="decs"> You have registered and the activation mail is sent to your email.Click the activation link to activate you account.</p><a href="/phpbasic3/">Về trang chủ</a>';
                                    $('#notification').html($content);
                                }
                                else {
                                    $('#notification').html(response);
                                }
                            }
                        });
                    }
                });
            }
            else {
                $('#notification').html('<li>Please tick verify checkbox.</li>');
            }

        }
    });
});

