$(document).ready(function () {
    $('#BTNactive').on('click', function () {
        var email = $('#email').val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if (!pattern.test(email)) {
            $content = '<h4 class="text-danger">Not a valid email</h4>';
            $('#notification').html($content);
        }
        else {
            $.ajax({
                url: 'reactive.php',
                type: 'post',
                data: {
                    type: 'reactive',
                    email: email
                },
                success: function (response) {
                    if (response == 1) {
                        $('#reActiveForm').hide();
                        $content = '<h1 class="text-success">Register Successed!</h1><p class="decs"> You have registered and the activation mail is sent to your email.Click the activation link to activate you account.</p><a href="/phpbasic3/">Về trang chủ</a>';
                        $('#notification').html($content);
                    }
                    else {
                        console.log('flase');
                    }
                }
            });
        }
    });
});