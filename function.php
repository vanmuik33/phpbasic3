<?php
if (session_id() === '')
    session_start();
function checkLoginType()
{
    if (isset($_SESSION['userID']) || isset($_SESSION['userFacebookID'])) {
        isset($_SESSION['userID']) ? $userID = $_SESSION['userID'] : $userID = $_SESSION['userFacebookID'];
        return $userID;
    } else return false;
}

class Redirect {
    public function __construct($url = null) {
        if ($url)
        {
            echo '<script>location.href="'.$url.'";</script>';
        }
    }
}