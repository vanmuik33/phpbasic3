<?php
function checkPermission($role)
{
    if (isset($_SESSION['userID']) == false) {
        header('location: /phpbasic3/signIn.php');
    } else {
        if (isset($_SESSION['permission']) == true) {
            // $permission =  explode(',', $_SESSION['permission']);
            if ($role == '') {
                echo 'Access denied. <br>';
                echo "<a href='/phpbasic3/'> Click để về lại trang chủ</a>";
                exit();
            } else {
                foreach ($role as $value) {
                    if (in_array($value, $_SESSION['permission']) == false) {
                        echo 'Access denied. <br>';
                        echo "<a href='/phpbasic3/'> Click để về lại trang chủ</a>";
                        exit();
                    }
                }
            }
        }
    }
}
