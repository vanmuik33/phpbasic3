<!DOCTYPE html>
<html>

<head>
  <title>
    My Name
  </title>
</head>

<body>
  <?php
  session_start();
  require_once 'vendor/autoload.php';
  $fb = new Facebook\Facebook([
    'app_id' => '692251158364236', // Replace {app-id} with your app id
    'app_secret' => 'b3bb658861b7dfaeea689ae422dfd2c7',
    'default_graph_version' => 'v3.2',
  ]);

  $helper = $fb->getRedirectLoginHelper();

  $permissions = ['email']; // Optional permissions
  $callbackUrl = htmlspecialchars('https://muinv.lahvui.xyz/phpbasic3/fb-callback.php');
  $loginUrl = $helper->getLoginUrl($callbackUrl, $permissions);

  echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
  ?>

</body>

</html>