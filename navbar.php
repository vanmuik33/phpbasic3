<?php
include_once('function.php');
$avatar_default = '/phpbasic3/avatar/default-avatar.jpg';

if ($userID = checkLoginType()) {
    $getAvatar_SQL = "SELECT `AVATAR` FROM `USERS` WHERE (`USER_ID` = '$userID' OR `USER_FB_ID` = '$userID')";
    if ($getAvatarDB = mysqli_query($connect, $getAvatar_SQL)) {
        $avatar = mysqli_fetch_assoc($getAvatarDB);
        if ($avatar['AVATAR'] != '')
            $avatar = $avatar['AVATAR'];
        else $avatar = $avatar_default;
    }
}

?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == TRUE) : ?>
                <a class="navbar-brand" href="/phpbasic3/admin">HOME</a>
            <?php else : ?>
                <a class="navbar-brand" href="/phpbasic3/">HOME</a>
            <?php endif; ?>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">

                <?php if (!isset($_SESSION['userID']) && !isset($_SESSION['userFacebookID'])) : ?>
                    <li><a href="signIn.php"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                    <li><a href="signUp.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <?php else : ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><img src="<?php echo isset($_SESSION['fb_access_token'])? $avatar: ('https://' . $_SERVER['SERVER_NAME'] . $avatar)?>" alt="" style="height:20px; border-radius:50%"> <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <?php if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == TRUE) : ?>
                                <li><a href="/phpbasic3/profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                                <li><a href="/phpbasic3/admin/listUser.php?page=1"><span class="glyphicon glyphicon-list-alt"></span> List user</a></li>
                                <li><a href="/phpbasic3/admin/posts.php?page=1"><span class="glyphicon glyphicon-file"></span> List post</a></li>
                                <li><a href="/phpbasic3/signOut.php"><span class="glyphicon glyphicon-log-out"></span> Sign out</a></li>
                            <?php else : ?>
                                <li><a href="/phpbasic3/profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                                <li><a href="/phpbasic3/signOut.php"><span class="glyphicon glyphicon-log-out"></span> Sign out</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>