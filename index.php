<?php
include_once('connect.php');
include_once('function.php');
if (isset($_SESSION['isAdmin'])) {
    header('location: admin');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>
    <?php include('bootstrap3.php'); ?>
</head>

<body>
    
    <?php include('navbar.php'); ?>
    <div class="container">
        <?php
        if (!$userID = checkLoginType()) echo '<h1 class="text-primary">HOMEPAGE</h1>';
        else {
            $userDB_sql = "SELECT `LAST_NAME` FROM `USERS` WHERE (`USER_ID` = '$userID' OR `USER_FB_ID` = '$userID')";
            $getUserDB = mysqli_query($connect, $userDB_sql);
            $userDB = mysqli_fetch_assoc($getUserDB);
            echo '<h2 class="text-success">Welcome ' . $userDB['LAST_NAME'] . ' xinh đẹp.!!!</h2>';
        }
        if (isset($_SESSION['updated'])) {
            echo '<h2 class="text-success"> Update profile successfully </h2>';
            unset($_SESSION['updated']);
        }
        ?>
    </div>
</body>

</html>