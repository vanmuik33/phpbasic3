$(document).ready(function () {
    $('#deleteEmployeeModal').on('show.bs.modal', function (e) {
        var useID = $(e.relatedTarget).data('user-id');
        $('input[name="userID"]').attr('value', useID);
    });
});