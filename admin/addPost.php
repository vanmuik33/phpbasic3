<?php
include_once('../connect.php');
include_once('../function.php');
// Nếu đăng nhập
if ($userID = checkLoginType()) {
    // Nếu tồn tại POST action
    if (isset($_POST['type']) && $_POST['type'] == 'add_post') {
        // Các biến xử lý thông báo
        $show_alert = '<script>$("#formAddPost .alert").removeClass("hidden");</script>';
        $hide_alert = '<script>$("#formAddPost .alert").addClass("hidden");</script>';
        $success = '<script>$("#formAddPost .alert").attr("class", "alert alert-success");</script>';
        $title_add_post = $_POST['title_add_post'];
        $slug_add_post = $_POST['slug_add_post'];
        $sql_add_post = "INSERT INTO `POSTS` (
            `TITLE`,
            `DESCR`,
            `THUMB`,
            `SLUG`,
            `KEYWORDS`,
            `BODY`,
            `AUTHOR_ID`,
            `STATUS`,
            `VIEW`,
            `DATE_POSTED`
        ) VALUES (
            '$title_add_post',
            '',
            '',
            '$slug_add_post',
            '',
            '',
            '$userID',
            '0',
            '0',
            '$date_current'
        )";
        $result = mysqli_query($connect, $sql_add_post);
        if ($result) {
            mysqli_close($connect);
            new Redirect($_DOMAIN.'posts.php'); // Trở về trang danh sách bài viết
        } 
    }
}
// Nếu không đăng nhập
// else {
//     new Redirect($_DOMAIN);
// }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include('../bootstrap3.php') ?>
</head>

<body>
    <?php include('../navbar.php'); ?>
    <div class="container">
        <?php
        echo
            '
        <a href="' . $_DOMAIN . 'posts.php" class="btn btn-default">
            <span class="glyphicon glyphicon-arrow-left"></span> Trở về
        </a> 
        <p class="form-add-post">
        <form method="POST" id="formAddPost" onsubmit="return false;">
            <div class="form-group">
                <label>Tiêu đề bài viết</label>
                <input type="text" class="form-control title" id="title_add_post">
            </div>
            <div class="form-group">
                <label>URL bài viết</label>
                <input type="text" class="form-control slug" placeholder="Nhấp vào để tự tạo" id="slug_add_post">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Tạo</button>
            </div>
            <div class="alert alert-danger hidden"></div>
        </form>
    </p>  
    ';
        ?>
    </div>
    <script src="../js/ceateSlug.js"></script>
    <script src="../ajax/addPost.js"></script>

</body>

</html>