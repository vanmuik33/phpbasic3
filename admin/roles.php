<?php
include_once('../connect.php');
$limit = 10;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$from = ($page - 1) * $limit;
$getDB_SQL = "SELECT * FROM `USERS` INNER JOIN `GROUP_USER` WHERE (`IS_DELETE` !=1 AND USERS.GROUP_ID = GROUP_USER.GROUP_ID) ORDER BY `USER_ID` LIMIT $from , $limit";
$getDB = mysqli_query($connect, $getDB_SQL);
$getDBcount = mysqli_query($connect, "SELECT COUNT(`USER_ID`) FROM `USERS` WHERE `IS_DELETE` !=1 ");
if (isset($_GET['liveSearch']) && $_GET['liveSearch'] != '') {
    $key = $_GET['liveSearch'];
    $getDB_SQL = "SELECT * FROM `USERS` INNER JOIN `GROUP_USER` WHERE (`IS_DELETE` !=1 AND USERS.GROUP_ID = GROUP_USER.GROUP_ID AND (`FIRST_NAME`= '$key' OR `LAST_NAME` = '$key')) ORDER BY `USER_ID` LIMIT $from , $limit";
    $getDB = mysqli_query($connect, $getDB_SQL);
    $getDBcount = mysqli_query($connect, "SELECT COUNT(`USER_ID`) FROM `USERS` WHERE (`IS_DELETE` !=1 AND  (`FIRST_NAME`= '$key' OR `LAST_NAME` = '$key'))");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Roles & Permissions</title>
    <?php include('../bootstrap3.php') ?>
    <link rel="stylesheet" href="../css/roles_style.css">
</head>

<body>
    <?php include('../navbar.php') ?>
    <div class="container">
        <div>
            <form action="">
                <div class="search">
                    <input name="liveSearch" class="prompt" type="text" placeholder="Search Name or Email" value="<?php if (isset($_GET['liveSearch']) && $_GET['liveSearch'] != '') echo $_GET['liveSearch'] ?>">
                    <button type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </form>

        </div>
        <form action="">
            <table class="table table-striped table-hover" id="userTable">
                <thead>
                    <tr>
                        <th class="stt">#</th>
                        <th class="info_fields">NAME</th>
                        <th>EMAIL</th>
                        <th id="groupID" class="roles">GROUP</th>
                        <th id="groupID" class="roles">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    if (mysqli_num_rows($getDB) != 0) {
                        while ($row = mysqli_fetch_array($getDB)) {
                    ?>
                            <tr id="<?php echo $row["USER_ID"]; ?>">
                                <td class="stt"><?php echo $i; ?></td>
                                <td class="info_fields"><?php echo $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"]; ?></td>
                                <td><?php echo $row["EMAIL"]; ?></td>
                                <td class="roles">
                                    <?php echo $row["GROUP_NAME"] ?>
                                </td>
                                <td class="roles">
                                    <a href="role-detail.php?userID=<?php echo $row['USER_ID'] ?>&pageID=<?php echo $_GET['page'] ?>" title="Edit">
                                        <span class="edit glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            $i++;
                        }
                    } else {
                        ?>
                        <tr><td colspan="6">No result</td></tr>
                    <?php
                    }
                    ?>
                    <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" class="form-control">
                </tbody>
            </table>
        </form>

        <?php
        $DBcout = mysqli_fetch_row($getDBcount);
        $totalUser = $DBcout[0];
        $totalPage = ceil($totalUser / $limit);
        $pagLink = "<ul class='pagination pull-right'>";
        for ($i = 1; $i <= $totalPage; $i++) {
            if ($page == $i) $active = 'active';
            else $active = '';
            if (isset($_GET['liveSearch']) && $_GET['liveSearch'] != '')
                $pagLink .= "<li class='page-item " . $active . "'><a class='page-link' href='roles.php?page=" . $i . "&liveSearch=" . $_GET['liveSearch'] . "'>" . $i . "</a></li>";
            else
                $pagLink .= "<li class='page-item " . $active . "'><a class='page-link' href='roles.php?page=" . $i . "'>" . $i . "</a></li>";
        }
        echo $pagLink . "</ul>";
        ?>
    </div>
    <script src="../ajax/liveSearch.js"></script>
</body>

</html>