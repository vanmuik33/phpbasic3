<?php
if (isset($_GET['type']) && $_GET['type'] == 'getUserDB') {
    include_once('../connect.php');
    $userID = $_GET['userID'];
    $getUserDB = mysqli_query($connect, "SELECT * FROM `USERS` WHERE `USER_ID` =" . $userID);
    $userDB = mysqli_fetch_assoc($getUserDB);
    echo json_encode($userDB);
    die();
}
if (isset($_POST['type']) && $_POST['type'] == 'editUser') {
    include_once('../connect.php');
    $userID = $_POST['userID'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['email'];
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $gender = $_POST['gender'];
    if (!empty($_POST['language'])) $language = implode(',', $_POST['language']);
    else $language = '';
    $error = '';
    $regex = "/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i";
    if (!preg_match($regex, $email)) {
        $error .= '<li>Email address not valid</li>';
    }
    if (mysqli_num_rows(mysqli_query($connect, "SELECT `EMAIL`,`USER_ID` FROM USERS WHERE (`EMAIL` = '$email' AND `USER_ID` != '$userID')")) > 0) {
        $error .= '<li>Email existed</li>';
    }
    if ($error != '') {
        echo $error;
        die();
    } else {
        $updateSQL_Syntax = "UPDATE `USERS` SET `FIRST_NAME` = '$firstName', `LAST_NAME` = '$lastName', `EMAIL` = '$email', `GENDER` = '$gender', `LANGUAGE` = '$language' WHERE `USER_ID` = '$userID'";
        $result = mysqli_query($connect, $updateSQL_Syntax);
        if ($result) {
            echo 1;
            die();
        }
        else {
            echo $result;
            die();
        }
    }
}
else {
    echo 0;
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

</body>

</html>