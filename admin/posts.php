<?php
include_once('../connect.php');
include('../function.php');
$limit = 5;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$from = ($page - 1) * $limit;
$getDB_SQL = "SELECT * FROM `POSTS` ORDER BY `ID_POST` LIMIT $from , $limit";
$getDB = mysqli_query($connect, $getDB_SQL);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List User</title>
    <?php include('../bootstrap3.php') ?>
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/fillData.js" type="text/javascript"></script>
</head>

<body>
    <?php include('../navbar.php') ?>
    <div class="container">
        <p id="success"></p>
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Manage <b>Users</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="addPost.php" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"> </span><span> Add New Post</span></a>
                        <!-- <a href="#deleteMultipleEmployeeModal" class="btn btn-danger" id="delete_multiple" data-toggle="modal"><span class="glyphicon glyphicon-minus-sign"> </span> <span> Delete</span></a> -->
                        <!-- <a href="roles.php?page=1" class="btn btn-info" id="e"><span class="glyphicon glyphicon-check"> </span> <span> Roles & Permissions</span></a> -->
                    </div>
                </div>
            </div>
            <form action="" id="userForm">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="selectAll" name="selectAll">
                                    <label for="selectAll"></label>
                                </span>
                            </th>
                            <th>#</th>
                            <th>Author</th>
                            <th>TITLE</th>
                            <th>VIEW</th>
                            <th>STATUS</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($row = mysqli_fetch_array($getDB)) {
                        ?>
                            <tr id="<?php echo $row["USER_ID"]; ?>">
                                <td>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" name="userCheckbox[]" value="<?php echo $row["USER_ID"]; ?>" class="user_checkbox">
                                        <label for="userCheckbox"></label>
                                    </span>
                                </td>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row["AUTHOR_ID"] ?></td>
                                <td><?php echo $row["TITLE"]; ?></td>
                                <td><?php echo $row["VIEW"]; ?></td>
                                <td><?php echo $row["STATUS"]; ?></td>
                                <td><?php echo $row["DATE_POSTED"]; ?></td>

                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                        <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" class="form-control">
                    </tbody>
                </table>
            </form>
            <div id="notification">
            </div>
        </div>
        <script>
            $(document).ready(function() {
                var checkbox = $('table tbody td:first-child .user_checkbox');
                $("#selectAll").click(function() {
                    if (this.checked) {
                        checkbox.each(function() {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function() {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function() {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>
</body>

</html>